import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParadoxProblemBTest {

    @Test
    public void findModificationLengthWithExampleInput1Returns3() {
        assertEquals(3, ParadoxProblemB.findModificationLength("AAAAA".toCharArray(), "AGCGAA".toCharArray()));
    }

    @Test
    public void findModificationLengthWithExampleInput2Returns4() {
        assertEquals(4, ParadoxProblemB.findModificationLength("GTTTGACACACATT".toCharArray(), "GTTTGACCACAT".toCharArray()));
    }

    @Test
    public void findModificationLengthWithSameStringReturns0() {
        assertEquals(0, ParadoxProblemB.findModificationLength("GTTTGACACACATT".toCharArray(), "GTTTGACACACATT".toCharArray()));
    }

    @Test
    public void findModificationLengthWithOnlyModificationAtTheStartReturnsLengthOfPrefix() {
        assertEquals(4, ParadoxProblemB.findModificationLength("AAAAAA".toCharArray(), "GTGTAAAAAA".toCharArray()));
    }

    @Test
    public void findModificationLengthWithOnlyModificationAtTheEndReturnsLengthOfSuffix() {
        assertEquals(4, ParadoxProblemB.findModificationLength("AAAAAA".toCharArray(), "AAAAAAGTGT".toCharArray()));
    }

    @Test
    public void findModificationLengthWithOnlyDeletionReturnsZero() {
        assertEquals(0, ParadoxProblemB.findModificationLength("AAAAAG".toCharArray(), "AG".toCharArray()));
    }

    @Test
    public void findModificationLengthWithOnlyAdditionReturnsAdditionLength() {
        assertEquals(3, ParadoxProblemB.findModificationLength("AG".toCharArray(), "AGAAA".toCharArray()));
    }

    @Test
    public void findModificationLengthWithX() {
        assertEquals(1, ParadoxProblemB.findModificationLength("AAAA".toCharArray(), "AGAAA".toCharArray()));
    }

    @Test
    public void findModificationLengthWithBothEmptyStringReturns0() {
        assertEquals(0, ParadoxProblemB.findModificationLength("".toCharArray(), "".toCharArray()));
    }

    @Test
    public void findModificationLengthWithModifiedEmptyStringReturnsLengthOfStartString() {
        assertEquals(0, ParadoxProblemB.findModificationLength("AAAAA".toCharArray(), "".toCharArray()));
    }

    @Test
    public void findModificationLengthWithOriginalEmptyStringReturnsLengthOfStartString() {
        assertEquals(5, ParadoxProblemB.findModificationLength("".toCharArray(), "AAAAA".toCharArray()));
    }

    @Test
    public void findModificationLengthWithModificationContainingOriginalAtBothEndsReturnsLengthAfterOriginal() {
        assertEquals(10, ParadoxProblemB.findModificationLength("AA".toCharArray(), "AAAAAGGGGGAA".toCharArray()));
    }
}