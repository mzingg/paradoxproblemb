import java.util.Scanner;

public class ParadoxProblemB {

    public static void main(String[] args) {
        int result = 0;

        Scanner sc = new Scanner(System.in);
        if (sc.hasNext()) {
            char[] originalDna = sc.next().toCharArray();
            if (sc.hasNext()) {
                char[] modifiedDna = sc.next().toCharArray();
                result = findModificationLength(originalDna, modifiedDna);
            }
        }

        System.out.println(result);
    }

    static int findModificationLength(char[] originalDna, char[] modifiedDna) {
        int leftLength = findLengthOfLeftSimilarSequence(originalDna, modifiedDna);
        int rightLength = findLengthOfRightSimilarSequence(originalDna, modifiedDna);

        // this catches the edge case when the modification contains the original at both ends
        if (leftLength + rightLength > originalDna.length) {
            rightLength = originalDna.length - leftLength;
        }

        // can be negative when strings are equals
        int sequenceLength = modifiedDna.length - rightLength - leftLength;

        return Math.max(sequenceLength, 0);
    }

    private static int findLengthOfLeftSimilarSequence(char[] originalDna, char[] modifiedDna) {
        int minLength = Math.min(originalDna.length, modifiedDna.length);
        int pos = 0;
        while (pos < minLength && originalDna[pos] == modifiedDna[pos]) {
            pos++;
        }
        return pos;
    }

    private static int findLengthOfRightSimilarSequence(char[] originalDna, char[] modifiedDna) {
        int posOriginal = originalDna.length - 1;
        int posModified = modifiedDna.length - 1;
        int modificationLength = 0;
        while (posOriginal >= 0 && posModified >= 0 && originalDna[posOriginal] == modifiedDna[posModified]) {
            posModified--;
            posOriginal--;
            modificationLength++;
        }
        return modificationLength;
    }
}
